<?php
namespace BeeJee\Core;

class Config
{
    /**
     * Contains application path, a foldername 'backend'
     * 
     * @var string
     */
    private static $app_path = null;

    /**
     * Contains base path upper folder 'backend'
     * 
     * @var string
     */
    private static $base_path = null;

    /**
     * Contains values of config files
     * 
     * @var array
     */
    private static $values = array();

    public static function initVars()
    {
        if (self::$app_path == null) {
            self::$app_path = __DIR__.'/../';
        }
        if (self::$base_path == null) {
            self::$base_path = __DIR__.'/../../';
        }
    }

    /**
     * Initialize main config and another config files
     * 
     * @param array $main_config
     * @param array $config_files
     * 
     * @return void
     */
    public static function init($main_config, $config_files = array())
    {
        if (empty($main_config) == true) {
            throw new \Exception("Empty main config file, check this.");
        } else {
            self::$values = $main_config;
            
            foreach ($config_files as $key => $value) {
                $config_file = require_once self::$base_path."config/$value.php";
                self::set($value, $config_file);
            }
        }
    }

    /**
     * Get the path for 'backend' directory
     * 
     * @return string
     */
    public static function getAppPath()
    {
        if (self::$app_path == null) {
            self::initVars();
        }
        return self::$app_path;
    }

    /**
     * Get the path upper 'backend' directory
     *  
     * @return string
     */
    public static function getBasePath()
    {
        if (self::$app_path == null) {
            self::initVars();
        }
        return self::$base_path;
    }

    /**
     * Set array of config files into one class manager for this
     * 
     * @param string $name
     * @param array  $values
     *
     * @return void
     */
    public static function set($name, $values)
    {
        self::$values[$name] = $values;
    }

    /**
     * Get config value for key
     * 
     * @param string $name
     * @param string $key
     * 
     * @return mixed
     */
    public static function get($name, $key)
    {
        if (empty(self::$values) == true) {
            $main_config = require_once self::getBasePath().'config/main.php';
            self::init($main_config);
        }

        if (array_key_exists($name, self::$values)) {
            if (array_key_exists($key, self::$values[$name])) {
                return self::$values[$name][$key];
            }
        } 
        return false;
    }

    /**
     * Get all values from key config
     * 
     * @param  string $name
     * @return mixed
     */
    public static function getAll($name)
    {
        if (array_key_exists($name, self::$values)) {
            return self::$values[$name];
        }
        return false;
    }
}