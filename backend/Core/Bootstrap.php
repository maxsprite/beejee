<?php
namespace BeeJee\Core;

use BeeJee\Core\Config;
use BeeJee\Core\View;
use BeeJee\Core\Route;

use Symfony\Component\HttpFoundation\Request;

class Bootstrap
{
    /**
     * App entry point 
     * 
     * @return void
     */
    public static function run()
    {
        // Set Request object and check for false result
        if (Http::setRequest(Request::createFromGlobals()) == false) {
            throw new \Exception("Error Processing Request");
        }

        $main_config  = require_once __DIR__.'/../../config/main.php';
        $config_files = require_once __DIR__.'/../../config/init.php';

        Config::init($main_config, $config_files);

        if (Config::get('site', 'debug') == true) {
            $whoops = new \Whoops\Run;
            $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
            $whoops->register();
        }

        require_once Config::getBasePath().'config/doctrine.php';

        if (Config::get('twig', 'enable') == true) {
            self::initTwig();
        } else {
            throw new \Exception("Use only twig templates for this version please.");
        }

        // Init RouteCollection object
        Route::init();

        // Include routing file
        require_once Config::getBasePath().'config/routes.php';

        // Check url path and use controller action for find route or 404
        Route::match();
    }

    /**
     * Initialize twig php template
     * 
     * @return void
     */
    private static function initTwig()
    {
        $loader     = new \Twig_Loader_Filesystem(Config::get('twig', 'template_path'));
        $twig_cache = false;

        if (Config::get('site', 'debug') == false) {
            $twig_cache = Config::get('twig', 'cache_path');            
        }
        View::setTwig(new \Twig_Environment($loader, [
            'cache' => $twig_cache,
        ]));
    }
}