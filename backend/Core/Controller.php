<?php
namespace BeeJee\Core;

class Controller
{
    /**
     * Render twig template or php file
     * 
     * @param  string $template
     * 
     * @return void
     */
    protected function render($template, $params = array())
    {
        echo View::render($template, $params);
    }
}