<?php
namespace BeeJee\Core;

use BeeJee\Core\View;
use BeeJee\Core\Http;

use Symfony\Component\Routing\Route as SymfonyRoute;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;

class Route
{
    /**
     * Contains route collection
     * 
     * @var object
     */
    private static $collection = null;

    /**
     * Init route collection object
     * 
     * @param object $object
     * 
     * @return void
     */
    public static function init()
    {
        if (self::$collection == null) {
            self::$collection = new RouteCollection();
        }
    }

    /**
     * Create http methods route
     * 
     * @param  string $path   url path
     * @param  string $name   url name
     * @param  array  $params controller and action
     * 
     * @return void
     */
    public static function add($path, $name, array $params, $method = 'GET')
    {
        $route = new SymfonyRoute(
                        $path, 
                        $params, 
                        array(), 
                        array(), 
                        '', 
                        array(), 
                        array($method)
                    );
        self::$collection->add($name, $route);
    }

    /**
     * Create GET http method route
     * 
     * @param  string $path   url path
     * @param  string $name   url name
     * @param  array  $params controller and action
     * 
     * @return void
     */
    public static function get($path, $name, array $params)
    {
        self::add($path, $name, $params);
    }

    /**
     * Create POST http method route
     * 
     * @param  string $path   url path
     * @param  string $name   url name
     * @param  array  $params controller and action
     * 
     * @return void
     */
    public static function post($path, $name, array $params)
    {
        self::add($path, $name, $params, 'POST');
    }

    public static function match()
    {
        $context = new RequestContext();
        $context->fromRequest(Http::getRequest());
        
        $matcher = new UrlMatcher(self::$collection, $context);

        try {
            $parameters   = $matcher->match(Http::getPath());   
            $route_params = $parameters;
            unset($route_params['use']);
            unset($route_params['_route']);
            $controller = explode('::', $parameters['use']);
            $action     = $controller[1];
            $controller = "BeeJee\\Controllers\\$controller[0]";
            $controller = new $controller;
            if (empty($route_params) == true) {
                call_user_func(array($controller, $action));
            } else {
                call_user_func_array(array($controller, $action), $route_params);
            }
        } catch (ResourceNotFoundException $e) {
            header('HTTP/1.1 404 Not Found');
            echo View::render('404');
            exit;
        } catch (MethodNotAllowedException $e) {
            header('HTTP/1.1 404 Not Found');
            echo View::render('404');
            exit; 
        }
    }
}