<?php
namespace BeeJee\Core;

use BeeJee\Core\Config;

class View
{
    /**
     * Contains twig object or null
     * 
     * @var object
     */
    private static $twig = null;

    /**
     * Set twig object into class variable
     * 
     * @param Twig_Environment $object
     *
     * @return void
     */
    public static function setTwig(\Twig_Environment $object)
    {
        self::$twig = $object;
    }

    /**
     * Get twig object
     * 
     * @return object
     */
    public static function getTwig()
    {
        return self::$twig;
    }

    /**
     * Render twig template
     * 
     * @param  string $template
     * @param  array  $params
     * 
     * @return mixed
     */
    public static function render($template, $params = array())
    {
        $template .= '.'.Config::get('site', 'template_extension');
        return View::$twig->render($template, $params);
    }
}