<?php
namespace BeeJee\Core;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class Model
{
    /**
     * Contains doctrine entiry manager
     * 
     * @var object
     */
    private static $entityManager = null;

    /**
     * Contains validator for model fields
     * 
     * @var object
     */
    private static $validator = null;

    public function __construct($data = array()) {
        $this->load($data);
    }
    
    public function load($data = array()) {
        if (empty($data) == false) {
            foreach ($data as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    /**
     * Set doctrine entity manager
     * 
     * @param EntityManager $object
     * 
     * @return void
     */
    public static function setEntityManager(EntityManager $object)
    {
        self::$entityManager = $object;
    }

    /**
     * Get doctrine entity manager
     * 
     * @return object
     */
    public static function getEntityManager()
    {
        return self::$entityManager;
    }

    /**
     * Save model using doctrine entiry manager
     * 
     * @return bool
     */
    public function save()
    {
        self::getEntityManager()->persist($this);
        if (self::getEntityManager()->flush()) {
            return true;
        }
        return false;
    }
    
    /**
     * Remove entity, using doctrine entiry manager
     * 
     * @return bool
     */
    public function remove()
    {
        self::getEntityManager()->remove($this);
        if (self::getEntityManager()->flush()) {
            return true;
        }
        return false;
    }

    /**
     * Set validator object
     * 
     * @return void
     */
    private function setValidator()
    {
        if (self::$validator == null) {
            self::$validator = Validation::createValidatorBuilder()
                ->addMethodMapping('validator')
                ->getValidator();
        }
    }

    /**
     * Validate instance model
     * 
     * @return mixed
     */
    public function validate()
    {
        self::setValidator();
        return self::$validator->validate($this);
    }
}