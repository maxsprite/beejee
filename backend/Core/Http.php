<?php
namespace BeeJee\Core;

class Http
{
    /**
     * Contains request object or null
     * 
     * @var object
     */
    private static $request = null;

    /**
     * Getter for request object
     * 
     * @return object
     */
    public static function getRequest()
    {
        return self::$request;
    }

    /**
     * Set request object
     * 
     * @param object $request
     * 
     * @return bool
     */
    public static function setRequest($request)
    {
        if (is_object($request)) {
            self::$request = $request;
            return true;
        }
        return false;
    }

    public static function getPath()
    {
        return self::getRequest()->getPathInfo();
    }
}