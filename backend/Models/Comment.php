<?php
namespace BeeJee\Models;

use BeeJee\Core\Model;
use BeeJee\Core\Config;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Intervention\Image\ImageManagerStatic as Image;

/**
 * @Entity
 * @Table(name="comments")
 */
class Comment extends Model
{
    /** 
     * @Id 
     * @Column(type="integer") 
     * @GeneratedValue
     * 
     * @var int
     */
    public $id;

    /**
     * @Column(type="boolean", nullable=false, options={"default":false})
     * 
     * @var bool
     */
    public $active;
    
    /**
     * @Column(type="boolean", nullable=false, options={"default":false})
     * 
     * @var bool
     */
    public $moderated;

    /**
     * @Column(type="string", unique=true, nullable=false)
     * 
     * @var string
     */
    public $email;

    /**
     * @Column(type="string", unique=true, nullable=false)
     * 
     * @var string
     */
    public $username;
    
    /**
     * @Column(type="string", nullable=true)
     * 
     * @var string
     */
    public $image;

    /**
     * @Column(type="text", nullable=false)
     * 
     * @var string
     */
    public $text;
    
    /**
     * @Column(type="datetime", nullable=false)
     * 
     * @var datetime
     */
    public $datetime;

    public function __construct($data = array())
    {
        parent::__construct($data);
        $this->active    = false;
        $this->moderated = false;
        $this->datetime  = new \DateTime();
    }
    
    public function setImage($file)
    {
        Image::configure(array('driver' => 'gd'));
        $extentions = [".gif",".jpg",".jpeg", ".png"]; 
        $file_ext   = strrchr($file['image']['name'], ".");
        $uploaddir  = Config::getBasePath().'public/uploads/';
        $filename   = uniqid('img_');
        $uploadfile = $uploaddir . $filename . $file_ext;
        
        if(in_array($file_ext, $extentions)) {
            if (move_uploaded_file($file['image']['tmp_name'], $uploadfile)) {
                $image = Image::make($uploadfile);
                if ($image->width() > 320 || $image->height() > 240) {
                    $image->resize(320, 240);
                    $image->save($uploadfile);   
                }
                $this->image = "/uploads/$filename".$file_ext;
                return true;
            } else {
                return false;
            }       
        }
        return false;
    }

    /**
     * Validate model fields, use before save
     * 
     * @param  ClassMetadata $metadata
     * 
     * @return mixed
     */
    public static function validator(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('email', new Assert\NotBlank());
        $metadata->addPropertyConstraint('username', new Assert\NotBlank());
        $metadata->addPropertyConstraint('text', new Assert\NotBlank());
    }
}