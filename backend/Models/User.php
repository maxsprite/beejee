<?php
namespace BeeJee\Models;

use BeeJee\Core\Model;

/**
 * @Entity
 * @Table(name="users")
 */
class User extends Model
{
    /** 
     * @Id @Column(type="integer") @GeneratedValue
     * 
     * @var int
     */
    protected $id;

    /**
     * @Column(type="boolean", nullable=false, options={"default":false})
     * 
     * @var bool
     */
    protected $admin;

    /**
     * @Column(type="string", length=32, unique=true, nullable=false)
     * 
     * @var string
     */
    public $username;
}