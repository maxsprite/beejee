<?php
namespace BeeJee\Controllers;

use BeeJee\Core\Controller;
use BeeJee\Models\Comment;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class CommentsController extends Controller
{
    public function create()
    {
        $comment       = new Comment($_POST);
        $errors        = $comment->validate();
        $json_response = [];
        if (count($errors) > 0) {
            $json_response['status'] = false;
            foreach ($errors as $key => $item) {
                $json_response['data'][$key]['field']   = $item->getPropertyPath();
                $json_response['data'][$key]['message'] = $item->getMessage();
            }
            if ($_FILES['image']['size'] > 0) {
                if (! $comment->setImage($_FILES)) {
                    $json_response['data'][]['message'] = 'File invalid.';
                }   
            }
            echo json_encode($json_response);
            exit;
        } else {
            try {
                if ($_FILES['image']['size'] > 0) {
                    if (! $comment->setImage($_FILES)) {
                        $json_response['status'] = false;
                        $json_response['data'][]['message'] = 'File invalid.';
                        echo json_encode($json_response);
                        exit;            
                    }   
                }
                $comment->save();
            } catch(UniqueConstraintViolationException $e) {
                $json_response['status'] = false;
                $json_response['data'][]['message'] = 'You left message before.';
                echo json_encode($json_response);
                exit;    
            } catch(\Exception $e) {
                $json_response['status']  = false;
                $json_response['data'][]['message'] = $e->getMessage();
                echo json_encode($json_response);
                exit;
            }
            $json_response['status']  = true;
            $json_response['message'] = 'Your message will be moderated, before publish in this page.';
            echo json_encode($json_response);
            exit;
        }
    }
}