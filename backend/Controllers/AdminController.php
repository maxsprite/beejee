<?php
namespace BeeJee\Controllers;

use BeeJee\Core\Controller;
use BeeJee\Core\Model;

use Symfony\Component\HttpFoundation\RedirectResponse;

class AdminController extends Controller
{
    public function __construct()
    {
        $realm = 'Restricted area';

        //user => password
        $users = array('admin' => '123');
        
        if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
            header('HTTP/1.1 401 Unauthorized');
            header('WWW-Authenticate: Digest realm="'.$realm.
                   '",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');
        
            die('Text to send if user hits Cancel button');
        }
        
        
        // analyze the PHP_AUTH_DIGEST variable
        if (!($data = $this->http_digest_parse($_SERVER['PHP_AUTH_DIGEST'])) ||
            !isset($users[$data['username']]))
            die('Wrong Credentials!');
        
        
        // generate the valid response
        $A1 = md5($data['username'] . ':' . $realm . ':' . $users[$data['username']]);
        $A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
        $valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);
        
        if ($data['response'] != $valid_response)
            die('Wrong Credentials!');
        
        // ok, valid username & password
        // continue
    }
    
    // function to parse the http auth header
    function http_digest_parse($txt)
    {
        // protect against missing data
        $needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
        $data = array();
        $keys = implode('|', array_keys($needed_parts));
    
        preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);
    
        foreach ($matches as $m) {
            $data[$m[1]] = $m[3] ? $m[3] : $m[4];
            unset($needed_parts[$m[1]]);
        }
    
        return $needed_parts ? false : $data;
    }
    
    /**
     * Show all comments for admin
     * 
     */
    public function index()
    {
        $comments = Model::getEntityManager()
                            ->getRepository('BeeJee\Models\Comment')
                            ->findAll();
                            
        return $this->render('admin/index', [
            'comments' => $comments
        ]);
    }
    
    /**
     * Edit page for single comment
     * 
     */
    public function edit($id)
    {
        $comment = Model::getEntityManager()
                            ->find('BeeJee\Models\Comment', $id);
                            
        return $this->render('admin/edit', [
            'comment' => $comment
        ]);
    }
    
    /**
     * Update single comment
     * 
     */ 
    public function update($id)
    {
        $comment = Model::getEntityManager()
                            ->find('BeeJee\Models\Comment', $id);
                            
        if ($_POST['email'] != $comment->email || $_POST['username'] != $comment->username || $_POST['text'] != $comment->text) {
            $_POST['moderated'] = true;
        }
        
        if (array_key_exists('active', $_POST)) {
            $_POST['active'] = true;
        } else {
            $_POST['active'] = false;
        }
                            
        $comment->load($_POST);
        $comment->save();
        
        return $this->render('admin/edit', [
            'comment' => $comment
        ]);
    }
    
    /**
     * Delete single comment
     * 
     */ 
    public function delete($id)
    {
        $comment = Model::getEntityManager()
                            ->find('BeeJee\Models\Comment', $id);
        
        $comment->remove();
        echo new RedirectResponse('/admin');
    }
}