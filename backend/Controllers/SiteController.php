<?php
namespace BeeJee\Controllers;

use BeeJee\Core\Controller;
use BeeJee\Core\Model;

class SiteController extends Controller
{
    public function index()
    {
        $comments = Model::getEntityManager()
                            ->getRepository('BeeJee\Models\Comment')
                            ->findBy(['active' => true], ['datetime' => 'desc']);
                            
        return $this->render('index', [
            'comments' => $comments
        ]);
    }
    
    public function sort($field, $direction)
    {
        $comments = Model::getEntityManager()
                            ->getRepository('BeeJee\Models\Comment')
                            ->findBy(['active' => true], [$field => $direction]);
                            
        return $this->render('index', [
            'comments' => $comments
        ]);
    }

    public function about()
    {
        return $this->render('about');
    }

    public function foo($param1, $param2)
    {
        return $this->render('test', [
            'param1' => $param1,
            'param2' => $param2
        ]);
    }
}