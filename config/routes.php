<?php
// Set your custom routes here
use BeeJee\Core\Route;
        
Route::get('/', 'home', [
    'use' => 'SiteController::index'
]);

Route::get('/sort/{field}/{direction}', 'sort', [
    'use' => 'SiteController::sort'
]);

Route::get('/about', 'about', [
    'use' => 'SiteController::about'
]);

Route::get('/admin', 'admin.home', [
    'use' => 'AdminController::index'
]);

Route::get('/admin/comment/{id}/edit', 'admin.comment.edit', [
    'use' => 'AdminController::edit'
]);

Route::post('/admin/comment/{id}/update', 'admin.comment.update', [
    'use' => 'AdminController::update'
]);

Route::get('/admin/comment/{id}/delete', 'admin.comment.delete', [
    'use' => 'AdminController::delete'
]);

Route::post('/comments/create', 'comments.create', [
    'use' => 'CommentsController::create'
]);

Route::post('/foo/{param1}/{param2}', 'foo', [
    'use' => 'SiteController::foo'
]);