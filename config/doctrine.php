<?php

use BeeJee\Core\Config;
use BeeJee\Core\Model;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$doctrine_config = Setup::createAnnotationMetadataConfiguration(
    array(Config::getAppPath()."Models"),
    Config::get('site', 'debug')
);

$doctrine_conn = array(
    'driver'   => Config::get('database', 'driver'),
    'user'     => Config::get('database', 'user'),
    'password' => Config::get('database', 'password'),
    'dbname'   => Config::get('database', 'name'),
);

Model::setEntityManager(EntityManager::create($doctrine_conn, $doctrine_config));