<?php
// Contains all main configs
return [
    'site' => [
        'debug' => true,
        'template_extension' => 'twig',
    ],
    'database' => [
        'host'     => 'localhost',
        'driver'   => 'pdo_mysql',
        'name'     => 'c9',
        'user'     => 'maxsprite',
        'password' => ''
    ],
    'twig' => [
        'enable'        => true,
        'template_path' => '../frontend/views',
        'cache_path'    => '../var/cache',
    ],
];