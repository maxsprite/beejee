<?php
// Register prefixes for PSR-4 autoload classes

use Symfony\Component\ClassLoader\Psr4ClassLoader;

$loader = new Psr4ClassLoader();
$loader->addPrefix('BeeJee', __DIR__.'/../backend');
$loader->register();