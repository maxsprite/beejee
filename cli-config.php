<?php

require_once "./vendor/autoload.php";
require_once "./config/autoload.php";
require_once "./config/doctrine.php";

use BeeJee\Core\Model;

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet(
            Model::getEntityManager()
        );