$(document).ready(function() {
    Vue.config.delimiters = ['${', '}']
    
    var preview_image = false;
    
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                preview_image = e.target.result;
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $('#form-file').change(function() {
        readURL(this)
    });

    var vue_messages = new Vue({
        el: '#message-list',
        data: {
            items: []
        }
    });
    
    var form_errors = new Vue({
        el: '#form-errors',
        data: {
            items: []
        },
        methods: {
            close: function(index) {
                this.items.splice(index, 1)
            }
        }
    });

    var form_message = new Vue({
        el: '#form-message',
        data: {
            errors: [
                {field: 'test', message: 'test1'}
            ]
        },
        methods: {
            previewMessage: function() {
                var email = this.email;
                var username = this.username;
                var text = this.text;
                var image = null;
                
                if (preview_image != false) {
                    image = '<img src="'+ preview_image +'" class="img-thumbnail">'
                }
                
                console.log(email);
                if (email == null) {
                    form_errors.items.push({
                        field: 'email',
                        message: 'This value should not be blank.'
                    });
                }
                if (username == null) {
                    form_errors.items.push({
                        field: 'username',
                        message: 'This value should not be blank.'
                    });
                }
                if (text == null) {
                    form_errors.items.push({
                        field: 'text',
                        message: 'This value should not be blank.'
                    });
                }
                
                if (email != null && username != null) {
                    vue_messages.items.push({
                        email: email,
                        username: username,
                        text: text,
                        image: image
                    });
                }
            }
        }
    });

    var form_success = new Vue({
        el: '#form-success',
        data: {
            items: []
        },
        methods: {
            close: function(index) {
                this.items.splice(index, 1)
            }
        }
    });

    $('#form-preview').click(function() {

    });
    
    $('input[type=checkbox]').click(function() {
        if (this.checked) {
            $(this).val(true);
        } else {
            $(this).val(false);
        }
    });

    $('#form-message').submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        
         $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                console.log(data);
                data = jQuery.parseJSON(data);

                if (data['status'] == false) {
                    $.each(data['data'], function(el, item) {
    
                        form_errors.items.push({
                            field: item.field,
                            message: item.message
                        });
                    });
                } else {
                    form_success.items.push({
                        message: data['message']
                    });
                }
            },
            error: function(data){
                console.log("error");
                console.log(data);
            }
        });
        
        // $.post($(this).attr('action'), {data: data}, function(data) {
            
        //     data = jQuery.parseJSON(data);

        //     if (data['status'] == false) {
        //         $.each(data['data'], function(el, item) {

        //             form_errors.items.push({
        //                 field: item.field,
        //                 message: item.message
        //             });
        //         });
        //     } else {
        //         form_success.items.push({
        //             message: data['message']
        //         });
        //     }
        // });
    });

});